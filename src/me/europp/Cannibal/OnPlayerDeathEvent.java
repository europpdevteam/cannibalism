package me.europp.Cannibal;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class OnPlayerDeathEvent implements Listener {

	public Cannibal plugin;

	public OnPlayerDeathEvent(Cannibal instance) {
		plugin = instance;

	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player dead = event.getEntity();
		Player killer = dead.getKiller();
		if (killer instanceof Player) {
			ItemStack item;
			Location loc = dead.getLocation();
			//item 1
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item1.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item1.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 2
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item2.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item2.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 3
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item3.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item3.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 4
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item4.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item4.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 5
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item5.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item5.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 6
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item6.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item6.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 7
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item7.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item7.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 8
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item8.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item8.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 9
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item9.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item9.Amount"));
			dead.getWorld().dropItem(loc, item);
			//item 10
			item = new ItemStack(plugin.getConfig().getInt(
					"DeathDrops.Item10.Id"), plugin.getConfig().getInt(
					"DeathDrops.Item10.Amount"));
			dead.getWorld().dropItem(loc, item);
		}

	}

}
