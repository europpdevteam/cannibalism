package me.europp.Cannibal;

import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Cannibal extends JavaPlugin {

	public static Cannibal plugin;
	public final Logger logger = Logger.getLogger("Minecraft");
	public final OnPlayerDeathEvent playerDeath = new OnPlayerDeathEvent(this);

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " version " + pdfFile.getVersion()
				+ " is disabled.");

	}

	@Override
	public void onEnable() {

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(playerDeath, this);
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " version " + pdfFile.getVersion()
				+ " is enabled.");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();

	}

}
