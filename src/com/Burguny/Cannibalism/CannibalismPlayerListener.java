/*package com.Burguny.Cannibalism;

import org.bukkit.Instrument;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CannibalismPlayerListener implements Listener  
{
	public static Cannibalism plugin;
	Player player;
	public CannibalismPlayerListener(Cannibalism instance)
	{
		plugin = instance;
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onPlayerDeathEvent (PlayerDeathEvent event) 
	{
		player = event.getEntity();
		
		if((player.getLastDamageCause() instanceof EntityDamageByEntityEvent))
		{
			EntityDamageByEntityEvent lastEvent = (EntityDamageByEntityEvent) player.getLastDamageCause();

			Entity attacker = lastEvent.getDamager();
			boolean skip = false;

			if (lastEvent.getCause() == DamageCause.PROJECTILE) 
			{
				if(((Projectile)attacker).getShooter() instanceof Player)
				{
					attacker = ((Projectile)attacker).getShooter();
				}
				else
				{
					skip = true;
				}
			}
			if(!(attacker instanceof Player))
			{
				skip = true;
			}

			if(!skip)
			{
				Player killer = (Player) attacker;
				if(!plugin.getConfig().getBoolean("Main.Use Permissions") || killer.hasPermission("cannibalism.kill"))
				{
					if(plugin.getConfig().getBoolean("Messages.Send To Killer"))
					{
						killer.sendMessage(replaceColors(plugin.getConfig().getString("Messages.To Killer").replace("!killed", player.getName()).replace("!killer", killer.getName())));
					}
					if(plugin.getConfig().getBoolean("Messages.Send To Killed"))
					{
						killer.sendMessage(replaceColors(plugin.getConfig().getString("Messages.To Killed").replace("!killed", player.getName()).replace("!killer", killer.getName())));
					}
					
					if(plugin.getConfig().getInt("On Kill.1Number") > 0)
					{
						int id =plugin.getConfig().getInt("OnKill.1Id");
						ItemStack item= new ItemStack(id , plugin.getConfig().getInt("On Kill.1Number"));
						player.getWorld().dropItem(player.getLocation(), item);
					}
					if(plugin.getConfig().getInt("On Kill.2Number") > 0)
					{
						int id =plugin.getConfig().getInt("OnKill.2Id");
						ItemStack item= new ItemStack(id , plugin.getConfig().getInt("On Kill.2Number"));
						player.getWorld().dropItem(player.getLocation(), item);
					}
					if(plugin.getConfig().getInt("On Kill.3Number") > 0)
					{
						int id =plugin.getConfig().getInt("OnKill.3Id");
						ItemStack item= new ItemStack(id , plugin.getConfig().getInt("On Kill.3Number"));
						player.getWorld().dropItem(player.getLocation(), item);
					}
					
	
					if(plugin.getConfig().getInt("On Kill.Sickness Time") > 0)
					{
						killer.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, plugin.getConfig().getInt("On Kill.Sickness Time")*200, 1));
						killer.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, plugin.getConfig().getInt("On Kill.Sickness Time")*200, 1));
						
					}
				}
			}
		}
	}
	
	static String replaceColors (String message) 
	{
		return message.replaceAll("(?i)&([a-f0-9])", "\u00A7$1");
	}
}
*/